﻿using UnityEngine;
using System.Collections;

public class JamObject : MonoBehaviour {

	[SerializeField]
	float speed;

	// Use this for initialization
	void Start () {
		InvokeRepeating("DestoryMyself", 5.0f,0);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(speed * Time.deltaTime, 0, 0);
	}

	void OnMouseDown()
	{
		Debug.Log(gameObject.name + "被點到");
		GameManager.ins.SetScore();
		DestoryMyself();
	}

	void DestoryMyself()
	{
		Destroy(this.gameObject);
	}
}
