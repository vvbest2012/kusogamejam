﻿using UnityEngine;
using System.Collections;

public class JamFactor : MonoBehaviour {

	//Groups
	public GameObject [] groups;

	public void spawnNext() {
		// Random Index
		int i = Random.Range (0, groups.Length);

		Instantiate (groups[i], transform.position, Quaternion.identity);
	}

	void Start() {
		// Spawn initial Group
		spawnNext ();
	}
	

}
