﻿using UnityEngine;
using System.Collections;

public class JamFactory : MonoBehaviour {
	public int RandomNumberMin = 3;
	public int RandomNumberMax = 10;
	public GameObject jam;
	public float delay_time;

	// Use this for initialization
	private void Start()
	{
		InvokeRepeating("CreateJam", delay_time, Random.Range(RandomNumberMin, RandomNumberMax)) ;
	}

	// Update is called once per frame
	private void Update()
	{


	}

	public GameObject CreateJam()
	{
		Vector3 holePos = this.transform.position;
		GameObject _jam = Instantiate(jam);
		_jam.transform.parent = this.transform;
		_jam.transform.position = holePos;
		_jam.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
		return _jam;
	}
}
