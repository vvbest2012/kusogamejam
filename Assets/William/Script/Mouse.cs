﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Mouse : MonoBehaviour
{
    private RectTransform myrect;
    private Button mybutton;
    Image myimage;
    public int RandomNumberMin = 3;
    public int RandomNumberMax = 10;
    private bool StillLive = true;
    private Vector3 OrigPos;
    private MouseFactory myMouseFactory;

    // Use this for initialization
    private void Start()
    {
        DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
        mybutton = GetComponent<Button>();
        myimage = GetComponent<Image>();
        StartCoroutine(ReadyToGo());
        myimage.enabled = false;
        OrigPos = this.transform.position;
        myMouseFactory = GetComponentInParent<MouseFactory>();
        GameManager.ins.mouseCallBack += this.GameOverCallBack;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void ShowMouse()
    {
        myimage.enabled = true;
        mybutton.enabled = true;
    }

    public void GetHurt()
    {
        Debug.Log("我被打中了");
        this.StillLive = false;
        GameManager.ins.SetScore();
        StartCoroutine(ReadyToRest());
       
    }

    private IEnumerator ReadyToRest()
    {
        mybutton.enabled = false;
        yield return new WaitForSeconds(0.2f);
        myMouseFactory.CreateMouse();
        GameManager.ins.mouseCallBack -= this.GameOverCallBack;
        Destroy(this.gameObject);
    }

    private IEnumerator ReadyToGo()
    {
        int i = Random.Range(RandomNumberMin, RandomNumberMax);
        yield return new WaitForSeconds(i);
        this.StillLive = true;
        ShowMouse();
        FlyTo(GetComponent<Image>());
    }
    public void GameOverCallBack()
    {
        GameManager.ins.mouseCallBack -= this.GameOverCallBack;
        myMouseFactory.CreateMouse();
        Destroy(this.gameObject);
    }
    public void FlyTo(Graphic graphic)
    {
        myimage.enabled = true;
        RectTransform rt = graphic.rectTransform;
        Color c = graphic.color;
        c.a = 0;
        graphic.color = c;                                                   //先将字体透明
        Sequence mySequence = DOTween.Sequence();                            //创建空序列
        Tweener move1 = rt.DOMoveY(rt.position.y + 10, 0.5f);                //创建向上移动的第一个动画
        Tweener move2 = rt.DOMoveY(rt.position.y, 0.5f);                //创建回來原點的第二个动画
        Tweener alpha1 = graphic.DOColor(new Color(c.r, c.g, c.b, 1), 0.5f); //创建Alpha由0到1渐变的动画
        Tweener alpha2 = graphic.DOColor(new Color(c.r, c.g, c.b, 0), 0.5f); //创建Alpha由1到0渐变的动画
        mySequence.Append(move1);                  //先添加向上移动的动画
        mySequence.Join(alpha1);                   //同时执行Alpha由0到1渐变的动画
        mySequence.AppendInterval(1);              //延迟1秒钟
        mySequence.Append(move2);                  //添加向回來原點的动画
        mySequence.Join(alpha2);                   //同时执行Alpha由1到0渐变的动画

        if (StillLive)
        { StartCoroutine(ReadyToGo()); }
    }
}