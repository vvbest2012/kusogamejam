﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager ins;
    public static int TotalScore;
    public Text ScoreText;
    public Text LimtTimer;
    public float firstLimtTime;
     float LimtTime ;
    bool isPuase=false;
    public GameObject GameOverPanel;
    public Text ShowYourFinalScore;

    

    // Use this for initialization
    private void Start()
    {
        ins = this;
        LimtTime = firstLimtTime;
    }

    // Update is called once per frame
    private void Update()
    {
        LimtTime -= Time.deltaTime;
        SetTimer();
        if (LimtTime <= 0&!isPuase)
        {
            isPuase = true;
               GameOver();
        }
    }
    public delegate void MouseCallBack();
    public MouseCallBack mouseCallBack;
    public void SetScore()
    {
        TotalScore += 1;
        ScoreText.text = TotalScore.ToString();
    }
    public void ReSetScore()
    {
        TotalScore = 0;
        ScoreText.text = TotalScore.ToString();
    }
    public void SetTimer()
    {
        LimtTimer.text = LimtTime.ToString();
    }
    public void Pause()
    {
        if (!isPuase)
        { Time.timeScale = 0; }
        else { Time.timeScale = 1; }
    }
    public void GameOver()
    {
        Time.timeScale = 0;
        GameOverPanel.SetActive(true);
        ShowYourFinalScore.text = "你得到"+ TotalScore+"分";
		if (mouseCallBack != null)
		{ mouseCallBack(); }
        // ShowScore(ShowYourFinalScore);
    }
    public void ShowScore(Graphic graphic)
    {
        
        RectTransform rt = graphic.rectTransform;
        Color c = graphic.color;
        c.a = 0;
        graphic.color = c;                                                   //先将字体透明
        Sequence mySequence = DOTween.Sequence();                            //创建空序列
        Tweener move1 = rt.DOMoveY(rt.position.y + 10, 0.5f);                //创建向上移动的第一个动画
        Tweener move2 = rt.DOMoveY(rt.position.y, 0.5f);                //创建回來原點的第二个动画
        Tweener alpha1 = graphic.DOColor(new Color(c.r, c.g, c.b, 1), 0.5f); //创建Alpha由0到1渐变的动画
        Tweener alpha2 = graphic.DOColor(new Color(c.r, c.g, c.b, 0), 0.5f); //创建Alpha由1到0渐变的动画
        mySequence.Append(move1);                  //先添加向上移动的动画
        mySequence.Join(alpha1);                   //同时执行Alpha由0到1渐变的动画
       // mySequence.AppendInterval(5);              //延迟1秒钟
      //  mySequence.Append(move2);                  //添加向回來原點的动画
       // mySequence.Join(alpha2);                   //同时执行Alpha由1到0渐变的动画
    }
    public void ReStart()
    {
        Time.timeScale = 1;
        GameOverPanel.SetActive(false);
        isPuase = false;
        ReSetScore();
        LimtTime = firstLimtTime;
       
    }
}