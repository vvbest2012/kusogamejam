﻿using System.Collections;
using UnityEngine;

public class MouseFactory : MonoBehaviour
{
    public GameObject mouse;

    // Use this for initialization
    private void Start()
    {
		InvokeRepeating("CreateMouse", 0.0f, 3.0f);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public GameObject CreateMouse()
    {
        Vector3 holePos = this.transform.position;
        holePos.y -= 9;
        GameObject mou = Instantiate(mouse);
        mou.transform.parent = this.transform;
        mou.transform.position = holePos;
        mou.transform.localScale =new Vector3(0.5f,0.5f,0.5f);
        return mou;
    }
}